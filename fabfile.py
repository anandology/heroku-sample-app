# fabfile to deploy to your own server
import os
import sys
from fabric.api import task, run, env, cd, sudo

host = os.getenv("RORODATA_HOST", "testapp.do.rorodata.net")
env.hosts = [host]
#env.user = "root"

@task
def hello():
    run("echo hello world!")

@task
def clone():
    with cd("/home/anand"):
        run("git clone https://gitlab.com/anandology/heroku-sample-app.git")

@task
def setup_supervisor():
    sudo("apt-get update")
    sudo("apt-get install -y supervisor")
    sudo("ln -sf /home/anand/heroku-sample-app/etc/supervisor.conf /etc/supervisor/conf.d/")
    sudo("supervisorctl reload")

@task
def update():
    with cd("/home/anand/heroku-sample-app"):
        run("git pull")

@task
def deploy():
    with cd("/home/anand/heroku-sample-app"):
        run("git pull")
        sudo("supervisorctl restart sample-app")

